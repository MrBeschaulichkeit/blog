class PostsController < ApplicationController
	before_action :authenticate_user!, except: [ :index, :show ]

	def new
		@post = Post.new
	end

	def index
		@posts = Post.all
	end

	def show
		@post = Post.find(params[:id])
	end

	def create
		@post = Post.new(post_params)

		if(@post.save)
			redirect_to @post
		else
			render 'new'
		end
	end

	def edit
		@post = Post.find(params[:id])
	end

	def update
		@post = Post.find(params[:id])

		if (@post.update(post_params))
			redirect_to @post
		else
			render 'edit'
		end
	end

	def destroy
		@post = Post.find(params[:id])
		@post.destroy

		redirect_to posts_path
	end

	def change_locale
		if I18n.locale = 'en'
			change_locale_ru
		else
			change_locale_en
		end
	end

	private def change_locale_en
		I18n.locale = 'en'
    	file_dir = Rails.root.join('config', 'locales', 'en.yml').to_s
    	file = YAML.load_file(file_dir)
    	@swch_lang = "Русский"
    end

    private def change_locale_ru
    	I18n.locale = 'ru'
    	file_dir = Rails.root.join('config', 'locales', 'ru.yml').to_s
    	file = YAML.load_file(file_dir)
    	@swch_lang = "English"
    end

	private def post_params
		params.require(:post).permit(:title, :body)
	end
end
